<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class TaskController extends Controller
{
    function list()
    {
        $data = Task::paginate(15);
        return view('list', ['tasks' => $data]);
    }

    function store(Request $request)
    {
        $request->validate([
            'information' => 'required',
        ]);

        $task = new Task;
        $task->information = $request->information;
        $task->save();

        return redirect()->back()->with('success', 'Task added to database successfully');
    }

    function edit(Request $request, $id)
    {
        $request->validate([
            'information' => 'required',
        ]);

        $task = Task::where('id', $id)->first();
        $task->information = $request->information;
        $task->save();

        return redirect()->back()->with('success', 'Task edited successfully');
    }

    function delete($id)
    {
        Task::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Task deleted successfully');
    }
}
