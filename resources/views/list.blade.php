<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    
        <link rel="stylesheet" href="{{ asset('style.css') }}">
    </head>
    <body>
        <div class="top">
            <div class="container text-center">
                <h3 class="white">Task List</h3>
                <p class="white">See all the saved todo tasks</p>
            </div>
        </div>

        <div class="container">

            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ \Session::get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            @endif

            <div class="row justify-content-end">
                <div class="AddBtn">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AddModal">Add Task +</button>
                </div>
            </div>

            <div class="row">

                @foreach($tasks as $task)
                    <div class="card">
                    	<div class="card-title">
                    		<span>{{$task->updated_at}}</span>
                    		<div class="float-right">
                                <button type="button" class="btn btn-secondary edit" data-id="{{ $task->id }}"><i class="fas fa-pencil-alt"></i></button>
                                <button type="button" class="btn btn-danger delete" data-id="{{ $task->id }}"><i class="fas fa-trash-alt"></i></button>
                    		</div>
                    	</div>
                    	<hr class="CardHr"/>
                    	<div class="card-body">
                    		<p class="text-justify">{{$task->information}}</p>
                    	</div>
                    </div>
                @endforeach

            </div>

            <hr class="BottomHr"/>

            <div class="row justify-content-center">
                <div class="col col-auto">{{$tasks->links()}}</div>
            </div>

        </div>

        <div class="modal fade" id="AddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="exampleModalLabel">Add Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="add" method="POST">

                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="form-group">
            	    			<label for="AddTextArea">Information</label>
            	    			<textarea class="form-control" id="AddTextArea" rows="3" name="information"></textarea>
            	    		</div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="exampleModalLabel">Edit Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="EditForm" action="edit" method="POST">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="modal-body">
                            <div class="form-group">
            	    			<label for="EditTextArea">Information</label>
            	    			<textarea class="form-control" id="EditTextArea" rows="3" name="information"></textarea>
            	    		</div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="exampleModalLabel">Delete Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="DeleteForm" action="edit" method="POST">

                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="modal-body">
                            Are you sure that you want to delete a task?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    <script>
        $(".edit").click(function() {
            const id = $(this).data('id');
            const text = $($(this).parents()[2].getElementsByClassName("card-body")[0]).find('p')[0].innerText

            $('#EditForm').attr('action', 'edit/'+ id);
            $("#EditTextArea").val(text);

            $("#EditModal").modal("show");
        });

        $(".delete").click(function() {
            const id = $(this).data('id');

            $('#DeleteForm').attr('action', 'delete/'+ id);

            $("#DeleteModal").modal("show");
        });
    </script>
</html>
